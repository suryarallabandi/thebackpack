<html>
    <head>
        <title>Demo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">
        <style>
            html{
                font-family: 'Dancing Script', cursive;
            }
            .head{
                
            }
            .container{
                width: 800px;
                margin: 0 auto;
            }
            .code{
                margin: 0 auto;
            }
            h1{
                text-align: center;
                font-size: 40px;
            }
        </style>
    </head>
<body>
    <div class="head">
    <h1>DevOps</h1>
    </div>
    <hr>
    <div class="container">
        <h1>Jenkins declarative script to build and deploy in apache tomcat</h1>
        <div class="code">
        <p>
            <code>
            pipeline {<br>
    agent { label 'Demo' }<br>
    tools { <br>
        maven 'mymvn'<br> 
        jdk 'myjdk' <br>
    }<br>
    stages {<br>
        stage ('Initialize') {<br>
            steps {<br>
                sh '''<br>
                    echo "PATH = ${PATH}"<br>
                    echo "M2_HOME = ${M2_HOME}"<br>
                ''' <br>
            }<br>
        }<br>
        stage('SCM Checkout'){<br>
            steps {<br>
                git 'https://gitlab.com/suryarallabandi/thebackpack.git'<br>
            }<br>
        }<br>
        stage ('Build') {<br>
            steps {<br>
                sh 'mvn -Dmaven.test.failure.ignore=true install'<br> 
            }<br>
        }<br>
        stage ('Package') {<br>
            steps {<br>
                sh 'mvn package'<br>
            }<br>
        }<br>
        stage ('Deploy to Tomcat'){<br>
            steps {<br>
                sshagent(['bbeae85b-bd03-4ef4-a8ef-2309e8502cb4']) {<br>
                    sh """<br>
           scp -o StrictHostKeyChecking=no target/*.war ubuntu@172.31.14.14:/home/ubuntu<br>
           ssh -o StrictHostKeyChecking=no ubuntu@172.31.14.14 'cp -r /home/ubuntu/*.war /user/local/tomcat/webapps'<br>
         """<br>
            }<br>
            }<br>
        }<br>
}<br>
}<br>
</code>
</p>
</div>
    </div>
</body>
</html>
